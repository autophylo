#!/bin/sh
# This is the main analyzer script, producing a phylogenetic tree
# from DNA set using given comparator method.
#
# Usage: ./run.sh COMPARATOR OUTTREE.PNG DNAFILE1 DNAFILE2...

cmp="$1"; shift
output="$1"; shift
scratch="/tmp/autophylo-scratch$$"
mkdir "$scratch" || exit 1

for file; do
	basename "$file" | sed 's/\.[^.]*$//'
done >"$scratch/dna-list"

for file; do echo "$file"; done |
	./matrix_gen.sh "$cmp" "$scratch" |
	tee "$scratch/matrix" |
	./matrix_to_tree.sh |
	./tree_render.sh "$scratch/dna-list" |
	cat >"$output"
