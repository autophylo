#!/bin/sh
# The diff-based DNA file comparator.

filea="$1"
numa="$2"
fileb="$3"
numb="$4"
scratch="$5/cmp_diff"

echo "$filea - $fileb" >&2

mkdir -p "$scratch"
[ -e "$scratch/$numa.dna" ] || cp "$filea" "$scratch/$numa.dna"
[ -e "$scratch/$numb.dna" ] || cp "$fileb" "$scratch/$numb.dna"
srcdir="$(pwd)"
cd "$scratch"

sed -e 's/./&\n/g' <"$numa.dna" >"$numa.dnal"
sed -e 's/./&\n/g' <"$numb.dna" >"$numb.dnal"
all_lines="$(cat "$numa.dnal" "$numb.dnal" | wc -l)"
diff -c "$numa.dnal" "$numb.dnal" | grep -i '^[+-\!] [actg]' >diff
diff_lines=$(($(grep -c "^[+-]" diff) + $(grep -c "^!" diff)/2))

echo "$diff_lines / $all_lines" | bc -l
