#!/bin/sh
# The GenCompress-based DNA file comparator.

filea="$1"
numa="$2"
fileb="$3"
numb="$4"
scratch="$5/cmp_gencompress"

echo "$filea - $fileb" >&2

mkdir -p "$scratch"
[ -e "$scratch/$numa.dna" ] || cp "$filea" "$scratch/$numa.dna"
[ -e "$scratch/$numb.dna" ] || cp "$fileb" "$scratch/$numb.dna"
srcdir="$(pwd)"
cd "$scratch"

# We need to compress A, AB and A|B.
gencompress() {
	"$srcdir/cmp_gencompress/gencompress" "$@" >/dev/null
}

[ -e "$numa.GEN" ] || gencompress "$numa.dna"
if ! [ -d "cat-$numa-$numb" ]; then
	mkdir "cat-$numa-$numb"
	cat "$numa.dna" "$numb.dna" >"cat-$numa-$numb/cat.dna"
	(cd "cat-$numa-$numb"; gencompress cat.dna)
fi
if ! [ -d "cond-$numa-$numb" ]; then
	mkdir "cond-$numa-$numb"
	(cd "cond-$numa-$numb"
	 ln "../$numa.dna" cond.dna
	 ln "../$numb.dna" .
	 gencompress "cond.dna" -c "$numb.dna")
fi

echo "1 - ($(stat -c %s "$numa.GEN") - $(stat -c %s "cond-$numa-$numb/cond.GEN")) / $(stat -c %s "cat-$numa-$numb/cat.GEN")" | bc -l
