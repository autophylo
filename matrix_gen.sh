#!/bin/sh
# Taking a list of DNA files (on stdin) and a comparator script
# and scratch directory name (as parameters), produce a DNA-DNA
# matrix listing real-valued [0,1] distance between each two DNA
# files (on stdout).

# The comparator script will get five parameters: two filenames
# to compare, interspersed with respective numbers of the samples,
# and a directory name it can use as a scratch area, for caching
# data between invocations. It is expected to output a single [0,1]
# real-valued number representing the distance (0 is same, 1 is
# totally dissimilar).

cmp="$1"
scratch="$2"

mkdir -p "$scratch"
cat >"$scratch/files"

in=0
while read i; do
	jn=0
	while read j; do
		echo "$("$cmp" "$i" $in "$j" $jn "$scratch") $in $jn"
		# echo "$in $jn ($cmp $i $in $j $jn $scratch)" >&2
		jn=$((jn+1))
	done <"$scratch/files"
	in=$((in+1))
done <"$scratch/files"
