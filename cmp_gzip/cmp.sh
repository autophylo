#!/bin/sh
# The gzip-based DNA file comparator.

filea="$1"
numa="$2"
fileb="$3"
numb="$4"
scratch="$5/cmp_gzip"

echo "$filea - $fileb" >&2

mkdir -p "$scratch"
[ -e "$scratch/$numa.dna" ] || cp "$filea" "$scratch/$numa.dna"
[ -e "$scratch/$numb.dna" ] || cp "$fileb" "$scratch/$numb.dna"
srcdir="$(pwd)"
cd "$scratch"

[ -e "$numa-$numb.dna" ] || cat "$numa.dna" "$numb.dna" >"$numa-$numb.dna"

[ -e "$numa.dna.gz" ] || gzip -c "$numa.dna" >"$numa.dna.gz"
[ -e "$numb.dna.gz" ] || gzip -c "$numb.dna" >"$numb.dna.gz"
[ -e "$numa-$numb.dna.gz" ] || gzip -c "$numa-$numb.dna" >"$numa-$numb.dna.gz"

echo "$(stat -c %s "$numa-$numb.dna.gz") / ($(stat -c %s "$numa.dna.gz") + $(stat -c %s "$numb.dna.gz"))" | bc -l
