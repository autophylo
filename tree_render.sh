#!/bin/sh
# Take a tree described by weighted edges (on stdin), producing
# a png render (on stdout). Uses a text file taken as a parameter
# for labelling the nodes.

cat |
	perl -nle 'BEGIN {
			print "graph philo {";
			our @l;
			open my $f, "'"$1"'" or die "$!";
			while (<$f>) { chomp; push @l, $_; print "$#l [label=\"$_\"]"; }
			close $f;
		}
		chomp; my ($w, $a, $b) = split / /;
		printf "\"%d\" -- \"%d\" [w=%f, label=\"%.3f\"]\n", $a, $b, $w, $w;
		END { print "}"; }' |
	dot -Tpng
