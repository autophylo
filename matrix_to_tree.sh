#!/bin/sh
# Normalize the matrix (on stdin), removing the diagonal and merging
# complementary cells and producing a minimum spanning tree with virtual
# nodes per component join (on stdout).

cat |
	egrep -v '^[^ ]* (.) \1$' | # weed out edges to self
	perl -nle 'BEGIN { our @w; }
		chomp; my ($w, $a, $b) = split / /;
		if (not defined $w[$b][$a]) { $w[$a][$b] = $w; next; }
		$w[$b][$a] = ($w[$b][$a] + $w) / 2;
		END { for my $a (0..$#w) {
			for my $b ($a+1..$#w) {
				print "$w[$a][$b] $a $b"
			}
		} }' | # merge A_ij and A_ji
	sort -f | # lowest weights first (they have highest priority)
	perl -nle 'BEGIN { our @c = 0..1000; our @v; my $vv = 0; }
		chomp; my ($w, $a, $b) = split / /;
		$c[$a] != $c[$b] or next;
		# Each graph component is either a singleton, or
		# has "virtual head", merging two sub-components;
		# virtual heads have negative id, assigned sequentially.
		my $va = $e[$c[$a]] || $a; # virtual nodes
		my $vb = $e[$c[$b]] || $b;
		#print "\n$a($c[$a]):$va -- $b($c[$b]):$vb";
		my $d = $c[$b];
		for (0..$#c) { $c[$_] == $d and $c[$_] = $c[$a]; }
		my $vc = --$vv;
		$e[$c[$a]] = $vc;
		$w /= 2;
		print "$w $va $vc";
		print "$w $vb $vc";
		' # remove reflexive edges and extract minimum spanning tree
