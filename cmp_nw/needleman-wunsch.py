#!/usr/bin/python

from numpy import *
import sys

def NeedlemanWunsch(seqA, seqB, match, mismatch, gap):
	a = len(seqA)
	b = len(seqB)
	F = zeros((a+1,b+1))
	for i in range(0, a):
		F[i,0] = gap * i
	for j in range(0, b):
		F[0,j] = gap * j

	for i in range(1, a+1):
		for j in range(1, b+1):
			F[i,j] = -10000

			if (seqA[i-1] == seqB[j-1]):
				chscore = match
			else:
				chscore = mismatch
			if (F[i-1,j-1] + chscore > F[i,j]):
				F[i,j] = F[i-1,j-1] + chscore

			if (F[i-1,j] + gap > F[i,j]):
				F[i,j] = F[i-1,j] + gap
			if (F[i,j-1] + gap > F[i,j]):
				F[i,j] = F[i,j-1] + gap
			# print i, j, F[i,j]
	# print "** ", a, b, F[a,b]
	return F[a,b]

print NeedlemanWunsch(sys.argv[1],sys.argv[2],1,0,-1)
