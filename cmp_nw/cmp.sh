#!/bin/sh
# The diff-based DNA file comparator.

filea="$1"
numa="$2"
fileb="$3"
numb="$4"
scratch="$5/cmp_diff"

echo "$filea - $fileb" >&2

mkdir -p "$scratch"
[ -e "$scratch/$numa.dna" ] || cp "$filea" "$scratch/$numa.dna"
[ -e "$scratch/$numb.dna" ] || cp "$fileb" "$scratch/$numb.dna"
srcdir="$(pwd)"
cd "$scratch"

if [ $numa -eq $numb ]; then
	echo 0
	exit
fi

(echo -n "1 - ";
 "$srcdir/cmp_nw/needleman-wunsch.py" "$(cat "$numa.dna")" "$(cat "$numb.dna")" | tr -d '\n';
 echo " / ($(cat "$numa.dna" "$numb.dna" | wc -c) - 2)") | bc -l

#cat "$numa.dna" "$numb.dna" >both.dna
#"$srcdir/cmp_nw/nw.pl" "$srcdir/cmp_nw/sm" both.dna |
#	perl -le '$a=<>; chomp $a; $b=<>; chomp $b; @a = split(//, $a); @b = split(//, $b); $c = 0;
#		for (0..length($a)) { $a[$_] eq $b[$_] or $c++; }
#		print $c / length($a);'
